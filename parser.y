
%{
    #include <vector>
    #include <cstring>
    #include <iostream>
    #include <map>

    #include "EspeceMoleculaire.h"
    #include "Reaction.h"

    // Variables :
    extern FILE *yyin;
    extern map<string, EspeceMoleculaire* > mapEspecesMoleculaires;
    extern vector<Reaction*> tabReactions;
    extern int m_diametre;
    //extern int nbReactions;
    
    // Méthodes :
    extern int yylex();
    extern int yyparse();
    extern void yyerror(const char *s);
    extern EspeceMoleculaire * getEM(string nameEM);
%}


%union {
	int entier;
	float flottant;
	char *chaine;
}


%token <entier> INT
%token <flottant> FLOAT
%token <chaine> IDENT


%token SPECIES
%token SIZE
%token SPEED
%token POP
%token DIAMETRE


%token SEMI			// ';'
%token VRG			// ','
%token LP RP		// '(' et ')'
%token LB RB		// '[' et ']'
%token EQUAL		// '='
%token PLUS			// '+'
%token ARROW		// '->'

%left SEMI
%left EQUAL
%left PLUS

%type <chaine> list_id other_mol

%%
// ----------------------- Rules -------------------------

/* 

	Ordre du programme : un programme est formé 
	-> d'un ensemble de définition de molécules, 
	-> d'un ensemble de spécifications (précisant la vitesse, la taille) pour chaque molécule, 
	-> et d'un ensemble de réaction moléculaire.

*/

prog:
 list_decls list_instrs

list_decls:
 decl
| decl list_decls

list_instrs:

| instr list_instrs

instr:
 react
| speed_molecule
| size_molecule
| pop_molecule
| diametre_molecule

// Définition des molecules :

decl:
 SPECIES list_id SEMI

list_id:
 IDENT {
    cout << "Ajout de la dernière espèce moléculaire : " << $1 << endl;
    //EspeceMoleculaire e($1);
    //mapEspecesMoleculaires[e.getName()] = e;
    mapEspecesMoleculaires[$1] = new EspeceMoleculaire($1);
}                    

| IDENT VRG list_id { 
    cout << "Ajout d'une nouvelle espèce moléculaire : " << $1 << endl;
    //EspeceMoleculaire e($1);
    //mapEspecesMoleculaires[e.getName()] = e;
    mapEspecesMoleculaires[$1] = new EspeceMoleculaire($1);
}         


// Spécifications des molecules :

size_molecule:
 SIZE LP IDENT RP EQUAL INT SEMI {
     //cout << "Id : " << $3 << " - size " << $6 << endl;
     map<string, EspeceMoleculaire* >::iterator it = mapEspecesMoleculaires.find($3);
     if (it != mapEspecesMoleculaires.end())
     {
         it->second->setSize($6);
     }
     else
     {
         cout << "Attention : Espèce Moléculaire " << $3 << " pas déclarée et donc non présente dans le conteneur." << endl;
     }

  }

speed_molecule:
 SPEED LP IDENT RP EQUAL FLOAT SEMI {
     map<string, EspeceMoleculaire* >::iterator it = mapEspecesMoleculaires.find($3);
     if (it != mapEspecesMoleculaires.end())
     {
         it->second->setSpeed($6);
     }
     else
     {
         cout << "Attention : Espèce Moléculaire " << $3 << " pas déclarée et donc non présente dans le conteneur." << endl;
     }
 }

| SPEED LP IDENT RP EQUAL INT SEMI {
     map<string, EspeceMoleculaire* >::iterator it = mapEspecesMoleculaires.find($3);
     if (it != mapEspecesMoleculaires.end())
     {
         it->second->setSpeed($6);
     }
     else
     {
         cout << "Attention : Espèce Moléculaire " << $3 << " pas déclarée et donc non présente dans le conteneur." << endl;
     }
 }

pop_molecule:
 POP LP IDENT RP EQUAL INT SEMI {
     map<string, EspeceMoleculaire* >::iterator it = mapEspecesMoleculaires.find($3);
     if (it != mapEspecesMoleculaires.end())
     {
         it->second->setPop($6);
     }
     else
     {
         cout << "Attention : Espèce Moléculaire " << $3 << " pas déclarée et donc non présente dans le conteneur." << endl;
     }
 }

diametre_molecule:
 DIAMETRE EQUAL INT SEMI {
     m_diametre = $3;
 }


// Expression des réactions :
react:
 IDENT other_mol ARROW IDENT other_mol LB FLOAT RB SEMI {
     // Test : Reécriture de la réaction :
     cout << $1 << " + " << $2 << " -> " << $4 << " + " << $5 << " [" << $7 << "] " << endl;
     
     // Récupération de l'Espece Moleculaire
     EspeceMoleculaire* reactif1 = getEM($1);
     EspeceMoleculaire* produit1 = getEM($4);

     // Récuperation de la probabilité de réaction
     float proba = $7;
     
     // ATTENTION : ne fonctionne plus et c'est normal
     // Récupération des indices des EM pour table d'indice de réaction :     
     //  int ind_reactif1 = reactif1->getNum();
     //  int ind_produit1 = produit1->getNum();
     //  cout << "Ind R1 : " << ind_reactif1 << endl;
     //  cout << "Ind P1 : " << ind_produit1 << endl;
     //  cout << "Probabilité : " << proba << endl;
    
     Reaction * r = new Reaction(proba, reactif1, produit1);
     
     // Verifer s'il y a un deuxième produit -> verifier la taille du string retourné
     string str_reactif2 = $2;
     string str_produit2 = $5;
     if (str_reactif2.size() != 0)
     {
         EspeceMoleculaire* reactif2 = getEM(str_reactif2);
         r->setReactif2(reactif2);

         // ATTENTION : ne fonctionne plus et c'est normal
         //int ind_reactif2 = reactif2->getNum();
         //cout << "Ind R2 : " << ind_reactif2 << endl;
     }

     if (str_produit2.size() != 0)
     {
         EspeceMoleculaire* produit2 = getEM(str_produit2);
         r->setProduit2(produit2);
         
         // ATTENTION : ne fonctionne plus et c'est normal
         //int ind_produit2 = produit2->getNum();
         //cout << "Ind P2 : " << ind_produit2 << endl;

     }

     //r.afficher();

     // Ajout dans un tableau dynamique (vector) pour le solveur 1
     tabReactions.push_back(r);

     // On incremente le nombre de réactions
     //nbReactions++;

 }

other_mol:
/* empty */ { 
    char * s = new char[1]; 
    strcpy(s, ""); 
    $$ = s; }
| PLUS IDENT {
    $$ = $2;
}

%%


