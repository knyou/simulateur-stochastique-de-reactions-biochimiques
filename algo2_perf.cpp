
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <map>
#include <cmath>
#include <unistd.h>
#include <chrono>

#include "Reaction.h"
#include "EspeceMoleculaire.h"
#include "Molecule.h"

using namespace std;

//rempli la vésicule avec les molécules positionnée aléatoirement 
void rempli_grille(vector <Molecule*> vec_mol, vector <Molecule*>*** grille, int diametre, int taille_mol_max){

	for(size_t i = 0; i < vec_mol.size(); i++) {
		Molecule* mol_en_traitement = vec_mol[i]; 
		float x_mol = mol_en_traitement->getX();
		float y_mol = mol_en_traitement->getY();
		float z_mol = mol_en_traitement->getZ();

		int x = (int)(x_mol) + diametre/2;
		x = x/taille_mol_max + 1;
		int y = (int)(y_mol) + diametre/2;
		y = y/taille_mol_max + 1;
		int z = (int)(z_mol) + diametre/2;
		z = z/taille_mol_max + 1;	


		grille[x][y][z].push_back(mol_en_traitement);	
		
	}


}
//supprime un élément de la grille
void supprime_element_grille(vector <Molecule*>*** grille, Molecule* mol, int diametre, int taille_mol_max) {

	int i = (int)mol->getX() + diametre/2;
	i = i/taille_mol_max + 1;
	int j = (int)mol->getY() + diametre/2;
	j = j/taille_mol_max + 1;
	int k = (int)mol->getZ() + diametre/2;
	k = k/taille_mol_max + 1; 

	vector<Molecule*> vec_encours = grille[i][j][k];

	for(size_t p = 0; p < vec_encours.size(); p++) {
	
		if(vec_encours[p] == mol) { grille[i][j][k].erase(grille[i][j][k].begin() + p); }

	}
	
}


//ajoute un élément dans la grille
void ajoute_element_grille(vector <Molecule*>*** grille, Molecule* mol, int diametre, int taille_mol_max) {

	int i = (int)mol->getX() + diametre/2;
	i = i/taille_mol_max + 1;
	int j = (int)mol->getY() + diametre/2;
	j = j/taille_mol_max + 1;
	int k = (int)mol->getZ() + diametre/2;
	k = k/taille_mol_max + 1;

	grille[i][j][k].push_back(mol); 

}


//test s'il y a choc entre 2 molécules

bool choc_molecules2(Molecule* mol_a, Molecule* mol_b) {
//une molecule ne se choc pas avec elle meme

float mol_a_x = mol_a->getX();
float mol_a_y = mol_a->getY();
float mol_a_z = mol_a->getZ();
float mol_b_x = mol_b->getX();
float mol_b_y = mol_b->getY();
float mol_b_z = mol_b->getZ();

float distance_2 = (mol_a_x - mol_b_x)*(mol_a_x - mol_b_x) + (mol_a_y - mol_b_y)*(mol_a_y - mol_b_y) + (mol_a_z - mol_b_z)*(mol_a_z - mol_b_z); 
float distance = sqrt(distance_2);
//la premiere condition est pour qu'une molecule ne se choque pas avec elle meme
//dans algo2, des qu'on commence à traiter une molecule pour reaction bi-moleculaire, 
//on met process à true.
if (((float)(mol_a->get_EM()->getSize())/2. + (float)(mol_b->get_EM()->getSize())/2.) >= distance) {

	return true;
												
}



return false;

}


//pour une case de la grille donnée test si une molécule en particulier entre en collision avec une autre.
//Si oui, effectue une des réactions (s'il y en a) entre les deux molécules
bool grille_case_choc(vector <Molecule*>*** grille, Molecule* mol, bool ordre_parcours, vector<Reaction * > ** TabIndex,
		      int i, int j, int k) {


	vector<Molecule*> case_1 =  grille[i][j][k];
	for(size_t compt = 0; compt < case_1.size(); compt++) {
		if(case_1[compt]->getProcess() == ordre_parcours && choc_molecules2(mol, case_1[compt])){
			vector <Reaction*> vec_react = TabIndex[case_1[compt]->getNumEspece()][mol->getNumEspece()];
					size_t taille_vec_react = 	vec_react.size();				
					float compteur_proba = 0.;
					for(size_t compt_2 = 0; compt_2< taille_vec_react; compt_2++) {

					Reaction* react = vec_react[compt_2];
					float proba_react = react->getProba(); 
					float random = ( (float)rand()/(float)RAND_MAX);
					//petit point technique					
					compteur_proba += proba_react;
					if(random <= compteur_proba) {
						case_1[compt]->setProcess(!ordre_parcours);
						EspeceMoleculaire* prod_1 = react->getProduit1();
						EspeceMoleculaire* react_2 = react->getReactif2();
						EspeceMoleculaire* react_1 = react->getReactif1();
						prod_1->setPop(prod_1->getPop()+1);	
						react_2->setPop(react_2->getPop()-1);
						react_1->setPop(react_1->getPop()-1);	
						mol->setEspeceMoleculaire(prod_1);
						mol->setNumEspece(prod_1->getNum());
						//changer les caracteristiques de r1 en celle de p1
						EspeceMoleculaire* prod_2 = react->getProduit2();
						if (prod_2 != NULL) {
						prod_2->setPop(prod_2->getPop()+1);
						case_1[compt]->setEspeceMoleculaire(prod_2);
						case_1[compt]->setNumEspece(prod_2->getNum());	


						}
						else {
							grille[i][j][k].erase(grille[i][j][k].begin() + compt);
							case_1[compt]->setExiste(false);
	
						}
						return true;


			
		} 
	}


}}

	return false;


}

//Pour une molécule donnée compare sa position avec celle des molécules des cases autour pour voir s'il y a collision
//à l'aide de la méthode ci-dessus.
bool grille_choc(vector <Molecule*>*** grille, Molecule* mol, bool ordre_parcours, vector<Reaction * > ** TabIndex, int diametre, int taille_mol_max) {

	int i = (int)mol->getX() + diametre/2;
	i = i/taille_mol_max + 1;
	int j = (int)mol->getY() + diametre/2;
	j = j/taille_mol_max + 1;
	int k = (int)mol->getZ() + diametre/2;
	k = k/taille_mol_max + 1;

	for(int q = i-1; q< i+2 ; q++) {

		for(int r = j-1 ; r<j+2 ; r++) {

			for(int s = k-1; s<k+2; s++) {

				if(grille_case_choc(grille,mol,ordre_parcours,TabIndex,q,r,s)) {return true;}
			
			}
	
	
		}



	}


	return false;

}


#define PI           3.14159265358979323846

//met le nom des molécules dans le csv
void put_name_csv2(const char* fichier_csv,map<string, EspeceMoleculaire *> mapEspecesMoleculaires)
{
	ofstream myfile;
	myfile.open(fichier_csv,  std::ofstream::out | std::ofstream::trunc);
	map<string, EspeceMoleculaire *>::iterator it = mapEspecesMoleculaires.begin();

	for (; it != mapEspecesMoleculaires.end(); ++it)
	{
		string name_em = it->second->getName();
		myfile << name_em;
		myfile << ",";
	}

	myfile << endl;
}
//met la population des molécules à un instant t dans le csv
void put_pop_csv2(const char* fichier_csv,map<string, EspeceMoleculaire *> mapEspecesMoleculaires)
{
	ofstream myfile;
	myfile.open(fichier_csv, ofstream::app);
	map<string, EspeceMoleculaire *>::iterator it = mapEspecesMoleculaires.begin();

	for (; it != mapEspecesMoleculaires.end(); ++it)
	{
		int pop_em = it->second->getPop();
		myfile << pop_em;
		myfile << ",";
	}

	myfile << endl;
}



//fait avancer une molécule.
//si ne sort pas de la vésicule et que random <= mol_av->get_EM()->getSpeed()
Molecule molecule_test_pas_de_temps_suivant2(Molecule* mol_av, float vitesse, int diametre) {
float random = ( (float)rand()/(float)RAND_MAX);

float angle_1 = (rand()/(float)RAND_MAX) *360.;
float angle_2 = (rand()/(float)RAND_MAX) *360.;

float dx = cos(PI*(angle_1/180.));
float dy = sin(PI*(angle_1/180.));
float dz = cos(PI*(angle_2/180.));

float x = vitesse * dx + mol_av->getX();
float y = vitesse * dy + mol_av->getY();
float z = vitesse * dz + mol_av->getZ();
if (x >= -((float)diametre/2.) &&  y >= -((float)diametre/2.) && z >= -((float)diametre/2.) &&
    x <= (float)diametre/2. &&  y <= (float)diametre/2. && z <= (float)diametre/2. 
    && random <= mol_av->get_EM()->getSpeed()) {
	
	Molecule mol_a_return(mol_av->get_EM(),mol_av->getNumEspece(),!mol_av->getProcess(),x,y,z);
	return mol_a_return;

}
return *mol_av;

}


void modifie_champs_pos_mol2(Molecule* mol, Molecule mol_test) {

mol->setPosition(mol_test.getX(),mol_test.getY(),mol_test.getZ());

}


//algo 2 performant qui commence par initialiser une grille en 3d
//une case de la grille est un vecteur de Molécule
//Lit le vecteur qui contient toutes les molécules dans un sens puis dans l'autre
//voir les commentaires de l'algo 2 normal qui lui ressemble beaucoup
void algo2_perf (vector <Molecule*> vec, int taille_mol_max, int diametre, float vitesse, int temps, vector<Reaction * > ** TabIndex, const char* fichier_csv, map<string, EspeceMoleculaire *> mapEspecesMoleculaires) {

	cout << endl;
	cout << "Début ALGO 2 perf ..." << endl<<endl;
	auto start = chrono::steady_clock::now();

	int taille = 0;
	if (diametre%taille_mol_max == 0) {taille = diametre/taille_mol_max;} 
	else {taille = (diametre/taille_mol_max) + 1;}

	vector<Molecule*> *** grille = new vector<Molecule*>**[taille+3];
    	for(int i = 0;i<taille + 3;++i)
    	{
        	grille[i]= new vector<Molecule*>*[taille+3];
        	for(int j = 0;j<taille+3;++j)
            		{grille[i][j]=new vector<Molecule*>[taille+3];}
    	}

	for(int i = 0;i<taille + 3;++i)
    	{
        	
        	for(int j = 0;j<taille+3;++j)
            		{
			
			for(int k = 0;k<taille+3;++k){

				grille[i][j][k] = vector<Molecule*>(0);


			}



		}
    	}
	
	rempli_grille(vec,grille,diametre,taille_mol_max);
	

put_name_csv2(fichier_csv,mapEspecesMoleculaires);
put_pop_csv2(fichier_csv,mapEspecesMoleculaires);

for(int pas_de_temps = 0; pas_de_temps < temps ; pas_de_temps++)
{	
	for (size_t i = 0; i< vec.size(); i++) {

		
		Molecule* mol_en_traitement = vec[i];
		
		if(!mol_en_traitement->getExiste()){vec.erase(vec.begin()+i);}

		else{

		if(mol_en_traitement->getProcess() == false) {
		//reaction monomoleculaire
		vector <Reaction*> vec_react_mono = TabIndex[0][mol_en_traitement->getNumEspece()];
		size_t taille_vec_mono_react = vec_react_mono.size();				
		float compteur_proba_mono = 0.;
		for(size_t k = 0; k< taille_vec_mono_react; k++) {

		Reaction* react = vec_react_mono[k];
		float proba_react = react->getProba(); 
		float random = ( (float)rand()/(float)RAND_MAX);
		//petit point technique					
		compteur_proba_mono += proba_react;
		if(random <= compteur_proba_mono) {
			mol_en_traitement->setProcess(true) ; 
			EspeceMoleculaire* prod_1 = react->getProduit1();
			EspeceMoleculaire* react_1 = react->getReactif1();
			prod_1->setPop(prod_1->getPop()+1);	
			react_1->setPop(react_1->getPop()-1);	
			mol_en_traitement->setEspeceMoleculaire(prod_1);
			mol_en_traitement->setNumEspece(prod_1->getNum());
						//changer les caracteristiques de r1 en celle de p1
			EspeceMoleculaire* prod_2 = react->getProduit2();
			if (prod_2 != NULL) {
				prod_2->setPop(prod_2->getPop()+1);
				Molecule* produit_2 = new Molecule(prod_2,prod_2->getNum(),true, mol_en_traitement->getX(),
								mol_en_traitement->getY(),mol_en_traitement->getZ());
				vec.push_back(produit_2);
				//vec_mol_taille++;
			}
		break;
		}
		}			

                }


	if (mol_en_traitement->getProcess() == false) {
//reaction bi-moleculaire
			mol_en_traitement->setProcess(true);			
			Molecule test = molecule_test_pas_de_temps_suivant2(mol_en_traitement,vitesse,diametre);
			if( !grille_choc(grille, mol_en_traitement, false, TabIndex,diametre,taille_mol_max)) {
				
				supprime_element_grille(grille,mol_en_traitement,diametre,taille_mol_max);
						
				modifie_champs_pos_mol2(mol_en_traitement, test);
				
				ajoute_element_grille(grille,mol_en_traitement,diametre,taille_mol_max);
							
			}
		

		}		
	
	}
	
	}
	

if(pas_de_temps%1000 == 0){ put_pop_csv2(fichier_csv,mapEspecesMoleculaires);}



	for (int i = (int) vec.size() - 1; i>-1 ; i--) {
		Molecule* mol_en_traitement = vec[i];


		if(!mol_en_traitement->getExiste()){vec.erase(vec.begin()+i);}

		else{		
				//reaction monomoleculaire
		if (mol_en_traitement->getProcess() == true) {
		vector <Reaction*> vec_react_mono = TabIndex[0][mol_en_traitement->getNumEspece()];
		size_t taille_vec_mono_react = vec_react_mono.size();				
		float compteur_proba_mono = 0.;
		for(size_t k = 0; k< taille_vec_mono_react; k++) {

		Reaction* react = vec_react_mono[k];
		float proba_react = react->getProba(); 
		float random = ( (float)rand()/(float)RAND_MAX);
		//petit point technique					
		compteur_proba_mono += proba_react;
		if(random <= compteur_proba_mono) {
			mol_en_traitement->setProcess(false) ; 
			EspeceMoleculaire* prod_1 = react->getProduit1();
			EspeceMoleculaire* react_1 = react->getReactif1();
			prod_1->setPop(prod_1->getPop()+1);	
			react_1->setPop(react_1->getPop()-1);	
			mol_en_traitement->setEspeceMoleculaire(prod_1);
			mol_en_traitement->setNumEspece(prod_1->getNum());
						//changer les caracteristiques de r1 en celle de p1
			EspeceMoleculaire* prod_2 = react->getProduit2();
			if (prod_2 != NULL) {
				prod_2->setPop(prod_2->getPop()+1);
				Molecule* produit_2 = new Molecule(prod_2,prod_2->getNum(),false, mol_en_traitement->getX(),
								mol_en_traitement->getY(),mol_en_traitement->getZ());
				//produit_2->setProcess(false);				
				vec.push_back(produit_2); //push back une molecule qui doit etre coordonnées au niveau du process
				//i++;				
				//vec_mol_taille++;
			}
		break;
		}
		}			
		}
		if (mol_en_traitement->getProcess() == true) {
			mol_en_traitement->setProcess(false);			
			Molecule test = molecule_test_pas_de_temps_suivant2(mol_en_traitement,vitesse,diametre);
			if( !grille_choc(grille, mol_en_traitement, true, TabIndex,diametre,taille_mol_max)) {
		
				supprime_element_grille(grille,mol_en_traitement,diametre,taille_mol_max);
				modifie_champs_pos_mol2(mol_en_traitement, test);
				ajoute_element_grille(grille,mol_en_traitement,diametre,taille_mol_max);
			
			}
			
		
		}			
			

		
		
		}

	}		
	
if(pas_de_temps%1000 == 0){ put_pop_csv2(fichier_csv,mapEspecesMoleculaires);}
	
	}

	auto end = chrono::steady_clock::now();
	cout << "FIN ALGO 2 PERF.";	
	cout << "Temps écoulé en seconds : " 
		<< chrono::duration_cast<chrono::seconds>(end - start).count()
		<< " sec "<< endl<<endl;


}
//mettre dans le csv pareil que algo1



