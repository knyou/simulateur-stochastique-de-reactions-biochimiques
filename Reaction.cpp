#include "Reaction.h"
/*
Reaction::Reaction()
{

}

Reaction::Reaction(float proba, EspeceMoleculaire *reactif1, EspeceMoleculaire *produit1)
{
    m_probabilite = proba;
    m_reactif1 = reactif1;
    m_produit1 = produit1;
}
*/
Reaction::Reaction(float proba, EspeceMoleculaire *reactif1, EspeceMoleculaire *produit1, EspeceMoleculaire *reactif2, EspeceMoleculaire *produit2)
{
    m_probabilite = proba;
    m_reactif1 = reactif1;
    m_produit1 = produit1;
    m_reactif2 = reactif2;
    m_produit2 = produit2;
}

Reaction::~Reaction()
{
    
}

void Reaction::afficher() const
{
    cout << "* Probabilité de la réaction -> " << getProba() << endl;
    cout << "avec les especes moleculaires suivantes :" << endl;

    // Affichage des réactifs
    cout << "Reactif(s) : " << endl;
    m_reactif1->afficher();
    cout << endl;
    if (m_reactif2 != 0) {
        m_reactif2->afficher();
        cout << endl;
    }
    
    // Affichage des produits
    cout << "Produits(s) : " << endl;
    m_produit1->afficher();
    cout << endl;
    if (m_produit2 != 0) {
        m_produit2->afficher();
        cout << endl;
    }
    
}

void Reaction::affichageMoleculaire() const
{
    if (m_reactif2 == 0 && m_produit2 == 0) {
        cout << m_reactif1->getName() << " -> " << m_produit1->getName() << " | ";
    }    
    else if (m_reactif2 == 0) {
        cout << m_reactif1->getName() << " -> " << m_produit1->getName() << " + " << m_produit2->getName() << " | ";
    }
    else if (m_produit2 == 0)
    {
        cout << m_reactif1->getName() << " + " << m_reactif2->getName() << " -> " << m_produit1->getName() << " | ";
    }
    else
    {
        cout << m_reactif1->getName() << " + " << m_reactif2->getName() << " -> " << m_produit1->getName() << " + " << m_produit2->getName() << " | ";
    }
    
}

// Getters et setters
float Reaction::getProba() const
{
    return m_probabilite;
}

EspeceMoleculaire * Reaction::getReactif1() const
{
    return m_reactif1;
}

EspeceMoleculaire * Reaction::getReactif2() const
{
    return m_reactif2;
}

EspeceMoleculaire * Reaction::getProduit1() const
{
    return m_produit1;
}

EspeceMoleculaire * Reaction::getProduit2() const
{
    return m_produit2;
}

void Reaction::setReactif2(EspeceMoleculaire *r)
{
    m_reactif2 = r;
}

void Reaction::setProduit2(EspeceMoleculaire *p)
{
    m_produit2 = p;
}
