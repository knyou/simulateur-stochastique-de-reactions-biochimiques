#include "Molecule.h"

#include <algorithm>
#include <random>
#include <ctime>

Molecule::Molecule(EspeceMoleculaire *espece, int numEspece, bool process, float x, float y, float z)
{
    m_espece = espece;
    m_numEspece = numEspece;
    m_process = process;
    m_x = x;
    m_y = y;
    m_z = z;
    existe = true;
}

Molecule::~Molecule()
{
    
}

// Méthodes
// bool position_exists(vector<Molecule> &tabMolecules, float x, float y, float z)
// {
//     auto it = find_if(tabMolecules.begin(), tabMolecules.end(), [x, y, z](auto const &m){ return(m.getX() == x && m.getY() == y && m.getZ() == z); });
//     if (it == tabMolecules.end()) {
//         // Aucune Molecule avec les memes coordonnées
//         return false;
//     }
//     else
//     {
//         // Au moins une molecule avec les memes coordonnees
//         return true;
//     }
// }

// inline float rand_float(float a, float b)
// {
// 	static std::mt19937 engine(std::time(nullptr));
// 	static std::uniform_real_distribution<float> dist(a, b);
// 	return dist(engine);
// }

// bool Molecule::setPosition(float x, float y, float z)
// {    
//     if (!position_exists(&tabMolecules, x, y, z)) {
//         setX(x);
//         setY(y);
//         setZ(z);
//         return true;
//     } 
//     else
//     {
//         return false;
//     }   
      
// }

// void Molecule::initPosition()
// {
//     float x = 0.0;
//     float y = 0.0;
//     float z = 0.0;
//     bool resultSetPosition = false;
//     do
//     {
//         x = rand_float(-50.0, 50.0);
//         y = rand_float(-50.0, 50.0);
//         z = rand_float(-50.0, 50.0);
//         resultSetPosition = setPosition(x, y, z);
//     } while (resultSetPosition == false);
    
// }


// Getters et setters

void Molecule::setPosition(float x, float y, float z) {

	m_x = x;
	m_y = y;
	m_z = z;


}


int Molecule::getNumEspece() const
{
    return m_numEspece;
}

void Molecule::setNumEspece(int num)
{
    m_numEspece = num;
}

bool Molecule::getProcess() const
{
    return m_process;
}

void Molecule::setProcess(bool process)
{
    m_process = process;
}

float Molecule::getX() const
{
    return m_x;
}

void Molecule::setX(float x)
{
    m_x = x;
}

float Molecule::getY() const
{
    return m_y;
}

void Molecule::setY(float y)
{
    m_y = y;
}

float Molecule::getZ() const
{
    return m_z;
}

void Molecule::setZ(float z)
{
    m_z = z;
}

EspeceMoleculaire * Molecule::get_EM() const
{
    return m_espece;
}

void Molecule::setEspeceMoleculaire(EspeceMoleculaire * em)
{
    m_espece = em;
}

bool Molecule::getExiste () const{

	return existe;


}

void Molecule::setExiste(bool ex) {

	existe = ex;

}
