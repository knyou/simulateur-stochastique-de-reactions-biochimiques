#include "EspeceMoleculaire.h"

// Constructeur par défaut demandé pour le conteneur map
EspeceMoleculaire::EspeceMoleculaire()
{

}

EspeceMoleculaire::EspeceMoleculaire(string name)
{
    m_name = name;
}

EspeceMoleculaire::EspeceMoleculaire(string name, int numEspece, int size, float speed, int pop)
{
    m_name = name;
    m_numEspece = numEspece;
    m_size = size;
    m_speed = speed;
    m_pop = pop;
}

EspeceMoleculaire::~EspeceMoleculaire()
{
    
}


// Affichage pour tests
void EspeceMoleculaire::afficher() const
{
    cout << "Espèce Moleculaire n° " << getNum() << " :" << getName() << endl;
    cout << "-> size : " << getSize() << endl;
    cout << "-> speed : " << getSpeed() << endl;
    cout << "-> population : " << getPop() << endl;
}

// Getters
string EspeceMoleculaire::getName() const
{
    return m_name;
}

int EspeceMoleculaire::getNum() const
{
    return m_numEspece;
}

void EspeceMoleculaire::setNum(int numEspece)
{
    m_numEspece = numEspece;
}

int EspeceMoleculaire::getSize() const
{
    return m_size;
}

void EspeceMoleculaire::setSize(int size)
{
    m_size = size;
}

float EspeceMoleculaire::getSpeed() const
{
    return m_speed;
}

void EspeceMoleculaire::setSpeed(float speed)
{
    m_speed = speed;
}

int EspeceMoleculaire::getPop() const
{
    return m_pop;
}

void EspeceMoleculaire::setPop(int pop)
{
    m_pop = pop;
}


/*
int main(int argc, char const *argv[])
{
    EspeceMoleculaire e1("H2O", 10, 25.6, 12000);

    e1.afficher();
    
    cout << "Modification..." << endl;
    e1.setSize(1500);
    e1.afficher();
    

    return 0;
}
*/