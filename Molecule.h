#ifndef DEF_MOLECULE
#define DEF_MOLECULE

#include "EspeceMoleculaire.h"

#include <vector>
#include <algorithm>

class Molecule
{
    private:
    
        EspeceMoleculaire *m_espece;

        // x, y et z -> coordonnées de la molecule dans l'espace
        float m_x;
        float m_y;
        float m_z;

        bool m_process;      // Vrai si la molecule a été traitée par le process
        int m_numEspece;     // Numéro d'espece pour Index
	bool existe;         //pour algo2_perf
    
    public:

        // Contructeurs et destructeur
        Molecule(EspeceMoleculaire *espece, int numEspece, bool process, float x = 0, float y = 0, float z = 0);
        ~Molecule();

        // Méthodes
        void setPosition(float, float, float);
        void initPosition();

        // Getters et setters
        int getNumEspece() const;
        void setNumEspece(int);
        
        bool getProcess() const;
        void setProcess(bool);       

        float getX() const;
        void setX(float);
        
        float getY() const;
        void setY(float);
        
        float getZ() const;
        void setZ(float);

	bool getExiste() const;
	void setExiste(bool);

        EspeceMoleculaire * get_EM() const;
        void setEspeceMoleculaire(EspeceMoleculaire *);
};




#endif
