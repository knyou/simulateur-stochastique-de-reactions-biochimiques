
%{#include <cstudio>
  #include <iostream>
  #include "parser.tab.h"
  using namespace std;
  extern int yylex();
%}


name_species    [a-zA-Z]+[0-9]*[a-zA-Z]*
floatant        [0-9]+\.[0-9]+
entier          [1-9][0-9]*


%%
[ \t\n]                   {}
"species"                   {return SPECIES;}
"speed"                     {return SPEED;}
"pop"                       {return POP;}
"size"                      {return SIZE;}
"diametre"                  {return DIAMETRE;}
"+"                      {return PLUS;}
"->"                        {return ARROW;}
";"                       {return SEMI;}
","                       {return VRG;}
"("                       {return LP;}
")"                       {return RP;}
"="                       {return EQUAL;}
"["                       {return LB ;}
"]"                       {return RB ;}
{name_species}   {yylval.chaine = strdup(yytext);return IDENT; }
{floatant}   { yylval.flottant = atof(yytext); return FLOAT; }
{entier}           {yylval.entier = atoi(yytext);return INT;}
 "//"[^\n]*     {}          
.                {}
%%



