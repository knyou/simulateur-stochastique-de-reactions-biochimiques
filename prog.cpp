#include <iostream>

#include "EspeceMoleculaire.h"
#include "Molecule.h"
#include "Reaction.h"

#include <map>
#include <vector>

using namespace std;

// Déclarations :
map<string, EspeceMoleculaire* > mapEspecesMoleculaires;
vector<Reaction*> tabReactions;
//int nbReactions = 0;
int m_diametre;


// Récupérer lexer et parser
extern int yylex();
extern int yyparse();
extern FILE *yyin;

extern void algo1(vector<Reaction*> tableau, int volume, const char *fichier_csv);
extern vector <Molecule*> init_emplacement_molecule (map<string, EspeceMoleculaire* > map_initial, int diametre);
extern void algo2 (vector <Molecule*> vec, int diametre, float vitesse, int temps, vector<Reaction * > ** TabIndex, 
	    const char* fichier_csv, map<string, EspeceMoleculaire *> mapEspecesMoleculaires);

extern void algo2_perf (vector <Molecule*> vec, int taille_mol_max, int diametre, float vitesse, int temps, vector<Reaction * > ** TabIndex, const char* fichier_csv, map<string, EspeceMoleculaire *> mapEspecesMoleculaires) ;


void yyerror(const char * s)
{
    cout << "Parser error ! Message : " << s << endl;
    exit(-1);
}

void afficherMapEM()
{

    for(auto const & em : mapEspecesMoleculaires)
    {
        em.second->afficher();
        cout << endl;
    }
    
}

void afficherReactions()
{
    for(size_t i = 0; i < tabReactions.size(); i++)
    {
        tabReactions[i]->afficher();
    }
    
}

void afficherIndexTab(vector<Reaction * > ** tabIndex, int nbReactions)
{
    cout << "NOMBRE DE REACTIONS = " << nbReactions << endl;
    for(int i = 0; i < nbReactions; i++)
    {
        for(int j = 0; j < nbReactions; j++)
        {
            if (tabIndex[i][j].size() == 1) {
                tabIndex[i][j][0]->affichageMoleculaire();
            }
            else if (tabIndex[i][j].size() > 1) {
                for(size_t k = 0; k < tabIndex[i][j].size(); k++)
                {
                    tabIndex[i][j][k]->affichageMoleculaire();
                    cout << " ; ";
                }
                
            }
            else
            {
                cout << "null | ";
            }  
        }
        cout << endl;
    }
}

int updateIndiceMapEM()
{
    int indice = 0;

    for(auto const & em : mapEspecesMoleculaires)
    {
        em.second->setNum(indice + 1);
        indice++;
    }
    
    return indice;    
}

EspeceMoleculaire * getEM(string nameEM)
{
    map<string, EspeceMoleculaire* >::iterator it = mapEspecesMoleculaires.find(nameEM);
    if (it != mapEspecesMoleculaires.end()) {
        return it->second;
    }
    else
    {
        cout << "Espèce Moléculaire non présente" << endl;
        return 0;
    }
    
}

int main(int argc, char *argv[])
{

    // EXECUTION : ./nom_prog <data_file> <num_solveur>

    // On vérifie que le bon nombre de paramètre en CLI est saisi
    if (argc != 3) {
        cout << "Erreur : Nombre de paramètres incorrect" << endl;
        cout << "Usage : " << argv[0] << " <data_file> <numéro_solveur>" << endl;
        cout << endl;
        cout << "Détails : " << endl;
        cout << "<data_file> : fichier d'entré contenant les déclarations de molécules, de leurs caractéristiques et les réactions possibles." << endl;
        cout << "<numéro_solveur> : 1 pour le solveur n°1 ou 2 pour le solveur n°2." << endl;
        return EXIT_FAILURE;
    } 

    // Ouverture du fichier passé en argv[1] pour le parser :
    FILE * myfile = fopen(argv[1], "r");
    if (!myfile) {
        perror("Echec de l'ouverture du fichier à parser");
        return EXIT_FAILURE;
    }
    else {
        cout << "Ouverture du fichier \"" << argv[1] << "\" - OK " << endl;
    }

    // Choix du solveur    
    if (argv[2] == string("1")) {
        // Execution du solveur 1
        cout << "Solveur 1 ..." << endl;

        // On parse le fichier d'entré
        yyin = myfile;
        yyparse();

        // Pour le solveur 1, pas besoin de tableau d'index
        // on peut donc ne pas mettre à jour les numEspeces dans la map
        updateIndiceMapEM();
        
        // Tests
        afficherMapEM();
        afficherReactions();

        char const *filename = argv[1];
        std::string targetFilename = std::string(filename);
        targetFilename += std::string(".csv");

        algo1(tabReactions, m_diametre, targetFilename.c_str());

    }
    else if(argv[2] == string("2")) {
        // Execution du solveur 2
        cout << "Solveur 2 ..." << endl;

        // Déclaration du tableau d'index
        //Reaction *** tabIndex;
        vector<Reaction * > ** tabIndex;

        // On parse le fichier d'entré
        yyin = myfile;
        yyparse();

        // Mise à jour des numEspece pour le tableau d'index
        int nbEM = updateIndiceMapEM();

        cout << "Nb EM = " << nbEM << endl;    

        // On incremente le nombre d'espèce moculaire car dans tableau d'index,
        // si il y a qu'un seul réactif alors le réactif2 est à l'indice 0
        nbEM = nbEM + 1;

        // Initialisation du tableau d'index
        // tabIndex = new Reaction**[nbEM];
        tabIndex = new vector<Reaction * > *[nbEM];
        for(int i = 0; i < nbEM; i++)
        {
            //tabIndex[i] = new Reaction*[nbEM];
            tabIndex[i] = new vector<Reaction *>[nbEM];
        }

        // TODO: Remplir le tableau d'index
        for(size_t i = 0; i < tabReactions.size(); i++)
        {
            // Récupération des numéros des Especes Moleculaire de la Reaction (uniquement réactifs)
            int id_reactif1 = tabReactions[i]->getReactif1()->getNum();
            cout << "REACTION n° " << i + 1 << endl;        
            cout << "ID React1 : " << id_reactif1 << endl;

            // Si le pointeur du réactif 2 est différent de 0 alors il y a un deuxième réactif
            if (tabReactions[i]->getReactif2() != 0) {
                int id_reactif2 = tabReactions[i]->getReactif2()->getNum();
                cout << "ID React2 : " << id_reactif2 << endl;
		
                tabIndex[id_reactif1][id_reactif2].push_back(tabReactions[i]);
                tabIndex[id_reactif2][id_reactif1].push_back(tabReactions[i]);
                                      
                
            }
            else
            {
               tabIndex[id_reactif1][0].push_back(tabReactions[i]);
               tabIndex[0][id_reactif1].push_back(tabReactions[i]);
               
            }
            
                
        }
	
	vector <Molecule*> vec_mol_init = init_emplacement_molecule (mapEspecesMoleculaires, m_diametre);
	//cout << vec_mol_init[0]->getX() << "  "  <<vec_mol_init[0]->getY() << "  " <<vec_mol_init[0]->getZ() << endl;
	//cout << vec_mol_init[1]->getX() << "  "  <<vec_mol_init[1]->getY() << "  " <<vec_mol_init[1]->getZ() << endl;
	//cout << vec_mol_init[2]->getX() << "  "  <<vec_mol_init[2]->getY() << "  " <<vec_mol_init[2]->getZ() << endl;
	algo2_perf (vec_mol_init, 10 ,m_diametre, 10. , 300000 , tabIndex, "algo2.csv", mapEspecesMoleculaires);
        // Tests
        afficherMapEM();
        afficherReactions();
        afficherIndexTab(tabIndex, nbEM);
    }
    else {
        cout << "Erreur : Choix du solveur incorrect" << endl;
        cout << "Usage : " << argv[0] << " <data_file> <numéro_solveur>" << endl;
        cout << endl;
        cout << "Détails : " << endl;
        cout << "<data_file> : fichier d'entré contenant les déclarations de molécules, de leurs caractéristiques et les réactions possibles." << endl;
        cout << "<numéro_solveur> : 1 pour le solveur n°1 ou 2 pour le solveur n°2." << endl;
        return EXIT_FAILURE;
    }
   
    //cout << "Diametre : " << m_diametre << endl;
    // map<string, EspeceMoleculaire* >::iterator it = mapEspecesMoleculaires.find("Es");
    // it->second->setPop(265890);
    
    //tabReactions[1]->getReactif1()->setPop(265894);
    //afficherReactions();

    cout << "Execution terminée !" << endl;
    return 0;
}
