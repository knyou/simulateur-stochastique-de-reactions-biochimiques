#include "Reaction.h"
#include "EspeceMoleculaire.h"
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <map>
#include <cmath>
#include <unistd.h>
#include <chrono>
using namespace std;
#define PI           3.14159265358979323846
#define ALPHA 7.4e-7
//algo2 : une case = 10nm = vitesse de diffusion max

extern map<string, EspeceMoleculaire *> mapEspecesMoleculaires;

int gen_rand(int a, int b)
{
	return rand() % (b - a) + a;
}

//algo qui permute aléatoirement un vector de reaction

vector<Reaction* > permutation_alea(vector<Reaction* > vec, vector<int>vec_inter, int taille)
{

	vector<Reaction* > vec_final(taille);
	
	while (taille > 0)
	{
		int aleatoire_2 = gen_rand(0, taille);
		vec_final[vec_inter[aleatoire_2]] = vec[taille-1];
		vec_inter.erase(vec_inter.begin() + aleatoire_2);
		taille--;
	}
	
	return vec_final;
}

void put_name_csv(const char* fichier_csv)
{
	ofstream myfile;
	myfile.open(fichier_csv,  std::ofstream::out | std::ofstream::trunc);
	map<string, EspeceMoleculaire *>::iterator it = mapEspecesMoleculaires.begin();

	for (; it != mapEspecesMoleculaires.end(); ++it)
	{
		string name_em = it->second->getName();
		myfile << name_em;
		myfile << ",";
	}

	myfile << endl;
}

void put_pop_csv(const char* fichier_csv)
{
	ofstream myfile;
	myfile.open(fichier_csv, ofstream::app);
	map<string, EspeceMoleculaire *>::iterator it = mapEspecesMoleculaires.begin();

	for (; it != mapEspecesMoleculaires.end(); ++it)
	{
		int pop_em = it->second->getPop();
		myfile << pop_em;
		myfile << ",";
	}

	myfile << endl;
}

//traité la variable diametre.
//tous les 100 pas de temps mettre le nombre de molecule dans le csv.
void algo1(vector<Reaction*> tableau, int diametre, const char* fichier_csv)
{	cout << endl;
	cout << "Début ALGO 1 ..." << endl<<endl;
	
	int taille = tableau.size();
	float nb_chocs_eff;
	int partie_entier_chocs;
	float apres_vrg_chocs;
	float alea_0_1;
	auto start = chrono::steady_clock::now();
	// Ouverture du fichier CSV pour écriture

	vector<int> vec_inter(taille);
	for (int i = 0; i < taille; i++)
	{
		vec_inter[i] = i;
	}	

	float volume = (4.*PI*((float)diametre/2000.)*((float)diametre/2000.)*((float)diametre/2000.))/3.;
		
	put_name_csv(fichier_csv);
	put_pop_csv(fichier_csv);
	for (int pas_de_temps = 0; pas_de_temps < 1000000; pas_de_temps++)
	{
		tableau = permutation_alea(tableau,vec_inter,taille);


		for (int i = 0; i < taille; i++)
		{

			Reaction * reaction_en_traitement = tableau[i];
			EspeceMoleculaire *r1 = reaction_en_traitement->getReactif1();
			EspeceMoleculaire *r2 = reaction_en_traitement->getReactif2();
			EspeceMoleculaire *p1 = reaction_en_traitement->getProduit1();
			EspeceMoleculaire *p2 = reaction_en_traitement->getProduit2();

			int pop_r1 = r1->getPop();
			int pop_p1 = p1->getPop();
			float prob_reaction = reaction_en_traitement->getProba();

			if (r2 != 0)
			{
				int pop_r2 = r2->getPop();
				nb_chocs_eff = (ALPHA * pop_r1 * pop_r2 * prob_reaction)/volume;						
				partie_entier_chocs = (int)(nb_chocs_eff);
				apres_vrg_chocs = nb_chocs_eff - partie_entier_chocs;
				alea_0_1 = (float)rand() / (float)RAND_MAX;
				
				if (alea_0_1 < apres_vrg_chocs)
				{
					partie_entier_chocs = partie_entier_chocs + 1;
				}

				r1->setPop(pop_r1 - partie_entier_chocs);
				r2->setPop(pop_r2 - partie_entier_chocs);
				p1->setPop(pop_p1 + partie_entier_chocs);

				if (p2 != 0)
				{
					int pop_p2 = p2->getPop();
					p2->setPop(pop_p2 + partie_entier_chocs);
				}
			}

			else
			{
				nb_chocs_eff = (float)pop_r1 * prob_reaction;
				partie_entier_chocs = (int)nb_chocs_eff;
				apres_vrg_chocs = nb_chocs_eff - (float)partie_entier_chocs;
				alea_0_1 = (float)rand() / (float)RAND_MAX;
				if (alea_0_1 < apres_vrg_chocs)
				{
					partie_entier_chocs = partie_entier_chocs + 1;
				}

				r1->setPop(pop_r1 - partie_entier_chocs);
				p1->setPop(pop_p1 + partie_entier_chocs);

				if (p2 != 0)
				{
					int pop_p2 = p2->getPop();
					p2->setPop(pop_p2 + partie_entier_chocs);
				}
			}
		}

		if (pas_de_temps % 100 == 0)
		{

			put_pop_csv(fichier_csv);
		}
	}
	auto end = chrono::steady_clock::now();
	cout << "FIN ALGO 1.  ";	
	cout << "Temps écoulé en seconds : " 
		<< chrono::duration_cast<chrono::seconds>(end - start).count()
		<< " sec "<< endl<<endl;
	
}
