all:
	flex -l lexer.l
	bison -vd parser.y 
	g++ -std=c++17 -pedantic -O3 -o prog lex.yy.c parser.tab.c EspeceMoleculaire.cpp Reaction.cpp Molecule.cpp algo1.cpp algo2.cpp algo2_perf.cpp prog.cpp -lm -ll

