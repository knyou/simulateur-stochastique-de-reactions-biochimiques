
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <map>
#include <cmath>
#include <unistd.h>
#include <chrono>

#include "Reaction.h"
#include "EspeceMoleculaire.h"
#include "Molecule.h"

using namespace std;

#define PI           3.14159265358979323846

//met le nom des Espece MOleculaire dans le csv
void put_name_csv(const char* fichier_csv,map<string, EspeceMoleculaire *> mapEspecesMoleculaires)
{
	ofstream myfile;
	myfile.open(fichier_csv,  std::ofstream::out | std::ofstream::trunc);
	map<string, EspeceMoleculaire *>::iterator it = mapEspecesMoleculaires.begin();

	for (; it != mapEspecesMoleculaires.end(); ++it)
	{
		string name_em = it->second->getName();
		myfile << name_em;
		myfile << ",";
	}

	myfile << endl;
}
//met les populations des mol dans le csv
void put_pop_csv(const char* fichier_csv,map<string, EspeceMoleculaire *> mapEspecesMoleculaires)
{
	ofstream myfile;
	myfile.open(fichier_csv, ofstream::app);
	map<string, EspeceMoleculaire *>::iterator it = mapEspecesMoleculaires.begin();

	for (; it != mapEspecesMoleculaires.end(); ++it)
	{
		int pop_em = it->second->getPop();
		myfile << pop_em;
		myfile << ",";
	}

	myfile << endl;
}




float genere_coord_alea (int diametre) {

float diam_f = (float) diametre;

	 return ( (float)rand()/(float)RAND_MAX ) * diam_f - diam_f/2.;

}
//dans molecule faire un setteur em;
//faire un getter espece
// mettre setteur et getteur m_process
// METTRE POINTEUR.


//initialise les molécules aléatoirement dans la vésicule
vector <Molecule*> init_emplacement_molecule (map<string, EspeceMoleculaire* > map_initial, int diametre) {

vector <Molecule*> vector_a_return(0);

map<string, EspeceMoleculaire *>::iterator it = map_initial.begin();

	for (; it != map_initial.end(); ++it)
	{
		EspeceMoleculaire* esp = it->second;
		int pop_espece = esp->getPop();
		int numero_espece = esp->getNum();
		for (int i = 0; i< pop_espece; i++) {
			float x_al = genere_coord_alea(diametre);
			float y_al = genere_coord_alea(diametre);
			float z_al = genere_coord_alea(diametre);
			Molecule* a = new Molecule(esp, numero_espece, false, x_al,y_al, z_al);
			vector_a_return.push_back(a); 
		
		}
	}
return vector_a_return;

}

//prend 2 molecules et vérifie si elles se sont choquées
bool choc_molecules(Molecule mol_a, Molecule mol_b) {
//une molecule ne se choc pas avec elle meme

float mol_a_x = mol_a.getX();
float mol_a_y = mol_a.getY();
float mol_a_z = mol_a.getZ();
float mol_b_x = mol_b.getX();
float mol_b_y = mol_b.getY();
float mol_b_z = mol_b.getZ();

float distance_2 = (mol_a_x - mol_b_x)*(mol_a_x - mol_b_x) + (mol_a_y - mol_b_y)*(mol_a_y - mol_b_y) + (mol_a_z - mol_b_z)*(mol_a_z - mol_b_z); 
float distance = sqrt(distance_2);
//la premiere condition est pour qu'une molecule ne se choque pas avec elle meme
//dans algo2, des qu'on commence à traiter une molecule pour reaction bi-moleculaire, 
//on met process à true.
if (((float)(mol_a.get_EM()->getSize())/2. + (float)(mol_b.get_EM()->getSize())/2.) >= distance) {

	return true;
												
}



return false;

}
//peut pas temponer une mol à true. -> ne bouge pas
//si collision on ne change pas la position des mol.

//prend une molecule et renvoie sa projection un pas de temps plus loin
//vérifie que ça ne sort pas de la vésicule
Molecule molecule_test_pas_de_temps_suivant(Molecule* mol_av, float vitesse, int diametre) {
float random = ( (float)rand()/(float)RAND_MAX);

float angle_1 = (rand()/(float)RAND_MAX) *360.;
float angle_2 = (rand()/(float)RAND_MAX) *360.;

float dx = cos(PI*(angle_1/180.));
float dy = sin(PI*(angle_1/180.));
float dz = cos(PI*(angle_2/180.));

float x = vitesse * dx + mol_av->getX();
float y = vitesse * dy + mol_av->getY();
float z = vitesse * dz + mol_av->getZ();
if (x >= -((float)diametre/2.) &&  y >= -((float)diametre/2.) && z >= -((float)diametre/2.) &&
    x <= (float)diametre/2. &&  y <= (float)diametre/2. && z <= (float)diametre/2. 
    && random <= mol_av->get_EM()->getSpeed()) {
	
	Molecule mol_a_return(mol_av->get_EM(),mol_av->getNumEspece(),!mol_av->getProcess(),x,y,z);
	return mol_a_return;

}
return *mol_av;

}


void modifie_champs_pos_mol(Molecule* mol, Molecule mol_test) {

mol->setPosition(mol_test.getX(),mol_test.getY(),mol_test.getZ());

}
/*
void bouge_molecule(Molecule* mol, float vitesse, int diametre) {

float angle_1 = (rand()/(float)RAND_MAX) *360.;
float angle_2 = (rand()/(float)RAND_MAX) *360.;

float dx = cos(PI*(angle_1/180.));
float dy = sin(PI*(angle_1/180.));
float dz = cos(PI*(angle_2/180.));

float x = vitesse * dx;
float y = vitesse * dy;
float z = vitesse * dz;
if (mol->getX() + x <= (float)diametre && mol->getY() + y <= (float)diametre && mol->getZ() + z <= (float)diametre ) {
	mol->set_Pos(mol->getX() + x,mol->getY() + y,mol->getZ() + z);
}
}
*/
//traite le tableau des molécules dans un sens puis dans l'autre
//pour chacun des cas, on vérifie pour chaque molécule (si elle n'est pas traitée) et on vérifie qu'elle ne se choque pas avec une autre
//(double boucle)
void algo2 (vector <Molecule*> vec, int diametre, float vitesse, int temps, vector<Reaction * > ** TabIndex, 
	    const char* fichier_csv, map<string, EspeceMoleculaire *> mapEspecesMoleculaires) {

put_name_csv(fichier_csv,mapEspecesMoleculaires);
put_pop_csv(fichier_csv,mapEspecesMoleculaires);

for(int pas_de_temps = 0; pas_de_temps < temps ; pas_de_temps++)
{	
	for (int i = 0; i< (int) vec.size(); i++) {
		Molecule* mol_en_traitement = vec[i];
		//on fait la suite si la molecule n'est pas traitée
		if(mol_en_traitement->getProcess() == false) {
		//on triate d'abord les reactions monomoleculaire 
		vector <Reaction*> vec_react_mono = TabIndex[0][mol_en_traitement->getNumEspece()];
		int taille_vec_mono_react = vec_react_mono.size();				
		float compteur_proba_mono = 0.;
		for(int k = 0; k< taille_vec_mono_react; k++) {

		Reaction* react = vec_react_mono[k];
		float proba_react = react->getProba(); 
		float random = ( (float)rand()/(float)RAND_MAX);
		//petit point technique
		//les probabilités s'additionnent!!!					
		compteur_proba_mono += proba_react;
		if(random <= compteur_proba_mono) {
			mol_en_traitement->setProcess(true) ; 
			EspeceMoleculaire* prod_1 = react->getProduit1();
			EspeceMoleculaire* react_1 = react->getReactif1();
			prod_1->setPop(prod_1->getPop()+1);	
			react_1->setPop(react_1->getPop()-1);	
			mol_en_traitement->setEspeceMoleculaire(prod_1);
			mol_en_traitement->setNumEspece(prod_1->getNum());
						//changer les caracteristiques de r1 en celle de p1
			EspeceMoleculaire* prod_2 = react->getProduit2();
			if (prod_2 != NULL) {// si il y 2 produits, il faut creer une nv molecule
				prod_2->setPop(prod_2->getPop()+1);
				Molecule* produit_2 = new Molecule(prod_2,prod_2->getNum(),true, mol_en_traitement->getX(),
								mol_en_traitement->getY(),mol_en_traitement->getZ());
				vec.push_back(produit_2);
				//vec_mol_taille++;
			}
		break;
		}
		}			

                }

//si la molecule n'est pas traitée
	if (mol_en_traitement->getProcess() == false) {
//reaction bi-moleculaire
			mol_en_traitement->setProcess(true);			
			bool choc_a_eu_lieu = false;
			Molecule test = molecule_test_pas_de_temps_suivant(mol_en_traitement,vitesse,diametre);
			for(int j = 0; j < (int) vec.size() ; j++) {
				if (choc_molecules(test, *vec[j])) { // deux mol. se choque ou pas si elles sont diff.
					if(vec[j]->getProcess() == false){
									
					vector <Reaction*> vec_react = TabIndex[vec[j]->getNumEspece()][mol_en_traitement->getNumEspece()];
					int taille_vec_react = 	vec_react.size();				
					float compteur_proba = 0.;
					for(int k = 0; k< taille_vec_react; k++) {

					Reaction* react = vec_react[k];
					float proba_react = react->getProba(); 
					float random = ( (float)rand()/(float)RAND_MAX);
					//petit point technique	pareil que plus haut				
					compteur_proba += proba_react;
					if(random <= compteur_proba) {
						vec[j]->setProcess(true) ; 
						EspeceMoleculaire* prod_1 = react->getProduit1();
						EspeceMoleculaire* react_2 = react->getReactif2();
						EspeceMoleculaire* react_1 = react->getReactif1();
						prod_1->setPop(prod_1->getPop()+1);	
						react_2->setPop(react_2->getPop()-1);
						react_1->setPop(react_1->getPop()-1);	
						mol_en_traitement->setEspeceMoleculaire(prod_1);
						mol_en_traitement->setNumEspece(prod_1->getNum());
						//changer les caracteristiques de r1 en celle de p1
						EspeceMoleculaire* prod_2 = react->getProduit2();
						if (prod_2 != NULL) {
						prod_2->setPop(prod_2->getPop()+1);
						vec[j]->setEspeceMoleculaire(prod_2);
						vec[j]->setNumEspece(prod_2->getNum());	


						}
						else {//on retire la molecule du vecteur si pas de 2eme produit 
							vec.erase(vec.begin() + j);

						}
						
						choc_a_eu_lieu = true;
						
						// pour le premier produit on change le m_espece et le numespece
						// pour le deuxieme soit on fait la meme chose, soit on on suppr la mol du vector
						break;						
					}
					
					}
					
					}
					
				}
				if(choc_a_eu_lieu) { break; }
			}//si la molecule que l'on regarde n'en a choquée aucune, elle peut avancer			
			if (!choc_a_eu_lieu) {
					
					modifie_champs_pos_mol(mol_en_traitement, test);

			}
		

		}		
	
	
	
	}
/*
cout<< vec[30]->getNumEspece() << "    " << &vec[30]<<endl;
cout << vec[30]->getX() << "  "  <<vec[30]->getY() << "  " <<vec[30]->getZ() << endl;*/
put_pop_csv(fichier_csv,mapEspecesMoleculaires);
//mettre dans le csv pareil que algo1


	for (int i = (int) vec.size() - 1; i>-1 ; i--) {
		
		Molecule* mol_en_traitement = vec[i];
				//reaction monomoleculaire
		if (mol_en_traitement->getProcess() == true) {
		vector <Reaction*> vec_react_mono = TabIndex[0][mol_en_traitement->getNumEspece()];
		int taille_vec_mono_react = vec_react_mono.size();				
		float compteur_proba_mono = 0.;
		for(int k = 0; k< taille_vec_mono_react; k++) {

		Reaction* react = vec_react_mono[k];
		float proba_react = react->getProba(); 
		float random = ( (float)rand()/(float)RAND_MAX);
		//petit point technique					
		compteur_proba_mono += proba_react;
		if(random <= compteur_proba_mono) {
			mol_en_traitement->setProcess(false) ; 
			EspeceMoleculaire* prod_1 = react->getProduit1();
			EspeceMoleculaire* react_1 = react->getReactif1();
			prod_1->setPop(prod_1->getPop()+1);	
			react_1->setPop(react_1->getPop()-1);	
			mol_en_traitement->setEspeceMoleculaire(prod_1);
			mol_en_traitement->setNumEspece(prod_1->getNum());
						//changer les caracteristiques de r1 en celle de p1
			EspeceMoleculaire* prod_2 = react->getProduit2();
			if (prod_2 != NULL) {
				prod_2->setPop(prod_2->getPop()+1);
				Molecule* produit_2 = new Molecule(prod_2,prod_2->getNum(),false, mol_en_traitement->getX(),
								mol_en_traitement->getY(),mol_en_traitement->getZ());
				//produit_2->setProcess(false);				
				vec.push_back(produit_2); //push back une molecule qui doit etre coordonnées au niveau du process
				//i++;				
				//vec_mol_taille++;
			}
		break;
		}
		}			
		}
		if (mol_en_traitement->getProcess() == true) {
			mol_en_traitement->setProcess(false);			
			bool choc_a_eu_lieu = false;
			Molecule test = molecule_test_pas_de_temps_suivant(mol_en_traitement,vitesse,diametre);
			for(int j = 0; j <(int) vec.size() ; j++) {

				if (choc_molecules(test, *vec[j])) {
					if(vec[j]->getProcess() == true ){					

					vector <Reaction*> vec_react = TabIndex[vec[j]->getNumEspece()][mol_en_traitement->getNumEspece()];
					int taille_vec_react = 	vec_react.size();				
					float compteur_proba = 0.;
					for(int k = 0; k< taille_vec_react; k++) {

					Reaction* react = vec_react[k];
					float proba_react = react->getProba(); 
					float random = ( (float)rand()/(float)RAND_MAX);
					//petit point technique					
					compteur_proba += proba_react;
					if(random <= compteur_proba) {
						vec[j]->setProcess(false);
						EspeceMoleculaire* prod_1 = react->getProduit1();
						EspeceMoleculaire* react_2 = react->getReactif2();
						EspeceMoleculaire* react_1 = react->getReactif1();
						prod_1->setPop(prod_1->getPop()+1);	
						react_2->setPop(react_2->getPop()-1);
						react_1->setPop(react_1->getPop()-1);	
						mol_en_traitement->setEspeceMoleculaire(prod_1);
						mol_en_traitement->setNumEspece(prod_1->getNum());
						//changer les caracteristiques de r1 en celle de p1
						EspeceMoleculaire* prod_2 = react->getProduit2();
						if (prod_2 != NULL) {
						prod_2->setPop(prod_2->getPop()+1);
						vec[j]->setEspeceMoleculaire(prod_2);
						vec[j]->setNumEspece(prod_2->getNum());	
						

						}
						else {
							//pareil que précédemment mais i-- à cause de l'ordre de lecture
							vec.erase(vec.begin() + j);
							i--;

						}
						// pour le premier produit on change le m_espece et le numespece
						// pour le deuxieme soit on fait la meme chose, soit on on suppr la mol du vector
						choc_a_eu_lieu = true;	
										
						break;						
					}
					}


					}
					
				
				}
				if(choc_a_eu_lieu) {break;}
					
					

				}
				if (!choc_a_eu_lieu) {
			
					modifie_champs_pos_mol(mol_en_traitement, test);

					}
			
		
			}			
			

		}		
	
	put_pop_csv(fichier_csv,mapEspecesMoleculaires);
	
	}
}
//mettre dans le csv pareil que algo1




