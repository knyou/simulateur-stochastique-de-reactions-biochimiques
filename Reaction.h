#ifndef DEF_REACTION
#define DEF_REACTION

#include "EspeceMoleculaire.h"

class Reaction
{

    private:
        
        // Au maximum 2 réactifs et 2 produits
        EspeceMoleculaire *m_reactif1, *m_reactif2;
        EspeceMoleculaire *m_produit1, *m_produit2;
        
        // Probabilité que la réaction ait lieu
        float m_probabilite;

    public:

        // Contructeurs et destructeur
        //Reaction();
        //Reaction(float proba, EspeceMoleculaire *reactif1, EspeceMoleculaire *produit1);
        Reaction(float proba, EspeceMoleculaire *reactif1, EspeceMoleculaire *produit1, EspeceMoleculaire *reactif2 = 0, EspeceMoleculaire *produit2 = 0);
        ~Reaction();

        // Méthodes
        void afficher() const;
        void affichageMoleculaire() const;

        // Getters et setters
        float getProba() const;

        EspeceMoleculaire * getReactif1() const;
        EspeceMoleculaire * getReactif2() const;
        EspeceMoleculaire * getProduit1() const;
        EspeceMoleculaire * getProduit2() const;

        void setReactif2(EspeceMoleculaire *);
        void setProduit2(EspeceMoleculaire *);
        

};

#endif