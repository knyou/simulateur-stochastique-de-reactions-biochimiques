#ifndef DEF_ESPECE_MOLECULAIRE
#define DEF_ESPECE_MOLECULAIRE

#include <string>
#include <iostream>

using namespace std;

class EspeceMoleculaire {

    private:

        string m_name;          // nom
        int m_numEspece;        // entier pour retrouver lors des reactions
        int m_size;             // diamètre
        float m_speed;          // vitesse
        int m_pop;              // population

    
    public:

        /*
            MODIFICATION :
            lorsqu'on parse la ligne species, créer uniquement des objets
            EM grace à leur nom, puis les stocker dans un conteneur.
            Ensuite lors du parce des specificités (size, speed, ...) rechercher
            dans le conteneur grace au nom et venir modifier avec un setter
        */

        // Constructeurs et destructeur
        EspeceMoleculaire();
        EspeceMoleculaire(string name);
        EspeceMoleculaire(string name, int numEspece, int size, float speed, int pop);
        ~EspeceMoleculaire();

        // Méthodes
        void afficher() const;

        // Getters et setters
        string getName() const;
        
        int getNum() const;
        void setNum(int);

        int getSize() const;
        void setSize(int);

        float getSpeed() const;
        void setSpeed(float);

        int getPop() const;
        void setPop(int);


};


#endif